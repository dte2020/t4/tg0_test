## COMMANDS 

***git pull*** - reply content from a remote repository from master branch

***git checkout -b 'branch_name'*** - creates a branch called 'branch_name' and switches to it

    git checkout master - switch to master branch

    git checkout -b 'branch_name' - creates a branch called 'branch_name'
    
    
***git add 'file'*** - adds to the repository a new local file called 'file'

***git commit -m 'This is my comment'*** - creates a comment ('This is my comment') to the current changes and files 

***git push*** - updates refs locally are pushed to the remote repository into master branch

***git status*** - shows the current status from the repositorie, such as posible changes or updates to files

# Example:


## Creating our branch and upload our files

**git checkout -b 'dani/trabajo_herramientas_parte1'**

    Switched to a new branch 'dani/trabajo_herramientas_parte1'
    touch this_is_my_file.txt && echo 'This is random text' >> this_is_my_file.txt
    git status
    On branch dani/trabajo_herramientas_parte1
        Changes not staged for commit:
        (use "git add <file>..." to update what will be committed)
        (use "git restore <file>..." to discard changes in working directory)
                modified:   branches.md
        Untracked files:
        (use "git add <file>..." to include in what will be committed)
                git_commands.md
                this_is_my_file.txt

    no changes added to commit (use "git add" and/or "git commit -a")

**git add git_commands.md this_is_my_file.txt**

    warning: LF will be replaced by CRLF in this_is_my_file.txt.
    The file will have its original line endings in your working directory

**git commit -m "git_commands file and this_is_my file were added, my part is complete!"**

    [dani/trabajo_herramientas_parte1 3441485] git_commands file and this_is_my file were added, my part is complete!
    2 files changed, 1 insertion(+)
    create mode 100644 git_commands.md
    create mode 100644 this_is_my_file.txt

**git push --set-upstream origin dani/trabajo_herramientas_parte1**

    Enumerating objects: 5, done.
    Counting objects: 100% (5/5), done.
    Delta compression using up to 8 threads
    Compressing objects: 100% (2/2), done.
    Writing objects: 100% (4/4), 394 bytes | 197.00 KiB/s, done.
    Total 4 (delta 1), reused 0 (delta 0)
    remote:
    remote: To create a merge request for dani/trabajo_herramientas_parte1, visit:
    remote:   https://gitlab.com/dte2020/t4/tg0_test/-/merge_requests/new?merge_request%5Bsource_branch%5D=dani%2Ftrabajo_herramientas_parte1
    remote:
    To https://gitlab.com/dte2020/t4/tg0_test.git
    * [new branch]      dani/trabajo_herramientas_parte1 -> dani/trabajo_herramientas_parte1
    Branch 'dani/trabajo_herramientas_parte1' set up to track remote branch 'dani/trabajo_herramientas_parte1' from 'origin'.

# Updating our master branch and repeat the process
**git checkout master**

    Switched to branch 'master'
    Your branch is up to date with 'origin/master'.


**git pull**

    remote: Enumerating objects: 1, done.
    remote: Counting objects: 100% (1/1), done.
    remote: Total 1 (delta 0), reused 0 (delta 0), pack-reused 0
    Unpacking objects: 100% (1/1), done.
    From https://gitlab.com/dte2020/t4/tg0_test
    4e052a2..36b836d  master     -> origin/master
    Updating 4e052a2..36b836d
    Fast-forward
    branches.md         |  4 ++--
    git_commands.md     | 57 +++++++++++++++++++++++++++++++++++++++++++++++++++++
    this_is_my_file.txt |  1 +
    3 files changed, 60 insertions(+), 2 deletions(-)
    create mode 100644 git_commands.md
    create mode 100644 this_is_my_file.txt