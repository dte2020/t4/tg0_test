
# Branches Config

In order to create local / remote branches to collaborate with the repository three types of branches will be avaliable:
- task/(username): New task added to the project, signed with task name (p.e: task/roles_assignment)
- update/(username): Update from current files, folders signed with update name (p.e: update/html_with_docker)

Developers can work from different branches, all of them attached to the main 'Master', branches will be commited and pushed in order to pull requests before merge them to 'Master' branch (@d.perezmoreno task)

Once the PR is accepted, remove the branch locally and create a new one.

> 'Master branch': Do not work on this branch directly, create your own branch to work on assigned tasks.

- Commits will be included to every push in order to understand modifications and features included. 
> Important for colaborative Power Point